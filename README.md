# Test

```plantuml
@startuml
actor Client
participant GeodasClient
participant GeodasServer
participant LayersCatalogue
database LayersDB

Client -> GeodasClient : Save Subscription
GeodasClient -> GeodasServer : saveLayer(layer: LayerInput)
GeodasServer -> LayersCatalogue : isLayerAvailable(layerId, aoi:AOI)
LayersCatalogue <-> LayersDB : layers

alt layer exists
    LayersCatalogue -> GeodasServer: true
    GeodasServer -> GeodasClient: LayerInfo
    GeodasClient -> Client: Layer Available

else layer does not exists
    LayersCatalogue -> Analytics: Authentication Failure
end

LayersCatalogue -> GeodasServer: LayerInfo[]
GeodasServer -> GeodasClient : LayerInfo[]
GeodasClient -> Client : <SubscriptionView />
@enduml
```